require("dotenv").config();
const nodemailer = require('nodemailer');
const fs = require('fs');
const tmp = require('tmp');

const { Bot, GrammyError, HttpError, session, Keyboard } = require("grammy");

/* =========== Вихідні дані =========== */

// Перелік наших ФОПів
const fopLabels = ["ТОВ Верест", "ФОП Страхоцінський В.В.", "ФОП Бернашевський О.В.", "ФОП Страхоцінська В.В.", "ФОП Бернашевський Я.О.", "ФОП Олійник Д.О."];

// Перелік пунктів меню
const menuLabels = ["Договір"];

// Строки оплати
const paymentLabels = ["100% передоплата", "По факту", "7 днів", "14 днів", "21 день", "Вказати свій варіант"]

// База даних користувачів
const users = [
	{
		"userName": "І-Віра",
		"userPhone": "380507455811",
		"id": ""
	},
	{
		"userName": "Михайлов Влад",
		"userPhone": "380979904341",
		"id": ""
	},
	{
		"userName": "І-Толік",
		"userPhone": "380973764647",
		"id": ""
	},
	{
		"userName": "Едік",
		"userPhone": "380670095570",
		"id": ""
	},
	{
		"userName": "Врублевський Едуард Анатолійович",
		"userPhone": "380987361363",
		"id": ""
	},
	{
		"userName": "Горний Юрій Юрійович ",
		"userPhone": "380683284966",
		"id": ""
	},
	{
		"userName": "Павлюк Павло Михайлович ",
		"userPhone": "380967102844",
		"id": ""
	},
	{
		"userName": "Сокальський Володимир Володимирович",
		"userPhone": "380671526707",
		"id": ""
	},
	{
		"userName": "Стайоха Антон Олександрович ",
		"userPhone": "380673805556",
		"id": ""
	},
	{
		"userName": "Стібак Павло Леонідович ",
		"userPhone": "380970004304",
		"id": ""
	},
	{
		"userName": "Фабіянов Віталій Анатолійович ",
		"userPhone": "380967465862",
		"id": ""
	},
	{
		"userName": "Фурман Інна Пилипівна ",
		"userPhone": "380987237765",
		"id": ""
	},
	{
		"userName": "Биндю Михайло Ігорович",
		"userPhone": "380685299776",
		"id": ""
	},
	{
		"userName": "Булгаков Сергій Ігорович",
		"userPhone": "380685228696",
		"id": ""
	},
	{
		"userName": "Їжак Андрій Миколайович",
		"userPhone": "380990831629",
		"id": ""
	},
	{
		"userName": "Омелянчук Іван Дмитрович",
		"userPhone": "380689783433",
		"id": ""
	},
	{
		"userName": "Онуфран Вадим Валерійович",
		"userPhone": "380964126380"
	},
	{
		"userName": "Скотніков Валерій Володимирович",
		"userPhone": "380676027744",
		"id": ""
	},
	{
		"userName": "Жовнір Олександр Олександрович",
		"userPhone": "380972069766",
		"id": ""
	},
	{
		"userName": "Онуфрійчук Олексій Андрійович ",
		"userPhone": "380678473624",
		"id": ""
	},
	{
		"userName": "Сцібак Денис Анатолійович ",
		"userPhone": "380680374979",
		"id": ""
	},
	{
		"userName": "Франко Оксана Олександрівна",
		"userPhone": "380969279420",
		"id": ""
	},
	{
		"userName": "Павлюк Сергій Михайлович",
		"userPhone": "380976450132",
		"id": ""
	},
	{
		"userName": "Севастьянова Аліна Никонівна",
		"userPhone": "380989789043",
		"id": ""
	},
	{
		"userName": "Борецький Володимир Валерійович",
		"userPhone": "380958354856",
		"id": ""
	},
	{
		"userName": "Дейдиш Вікторія Леонідівна",
		"userPhone": "380985300204",
		"id": ""
	},
	{
		"userName": "Корольков Олександр Олександрович ",
		"userPhone": "380687226116",
		"id": ""
	},
	{
		"userName": "Раку Андрій Петрович",
		"userPhone": "380971474597",
		"id": ""
	},
	{
		"userName": "Севастьянов Ардан Едуардович",
		"userPhone": "380967377898",
		"id": ""
	},
	{
		"userName": "Безуглова Тетяна Ігорівна",
		"userPhone": "380685299976",
		"id": ""
	},
	{
		"userName": "Доротюк Олена Степанівна",
		"userPhone": "380977745349",
		"id": ""
	},
	{
		"userName": "Кугай Аліна Олексіївна",
		"userPhone": "380982605800",
		"id": ""
	},
	{
		"userName": "Полянська Оксана Анатоліївна",
		"userPhone": "380961564178",
		"id": ""
	}
]

// Стан користувачів
const userStates = {
};

/* =========== Бот =========== */

//Змінна для того, щоб віслідковувати на яке питання відповідає користувач
let marker = null;

// Створення екземпляру бота
const bot = new Bot("6821262991:AAEY8aMhhfFPH5yhyg-Pb9FcHdKyOB3d81M");
bot.use(session());

// Створення команд
bot.api.setMyCommands([
	{
		command: "start",
		description: "Запустити бота",
	},
	{
		command: "auth",
		description: "Авторизуватися",
	}
]);

// Якщо користувач ввів команду "start"
bot.command("start", async (ctx) => {
	userData = {
		fop: '',
		counterparyName: '',
		isNetwork: '',
		networkName: '',
		payment: '',
	}
	marker = null;

	const rows = menuLabels.map(label => [Keyboard.text(label)]);
	const menuKeyboard = Keyboard.from(rows).resized().oneTime();

	await ctx.reply("Бот запущений! \nОберіть необхідний пункт з меню.", {
		reply_markup: menuKeyboard
	});
});

// Якщо користувач ввів команду "start"
bot.command("auth", async (ctx) => {
	await ctx.reply("Будь ласка, надішліть свій номер телефону", {
		reply_markup: {
			keyboard: [
				[{ text: "Надіслати номер телефону", request_contact: true }],
			],
			resize_keyboard: true,
			one_time_keyboard: true,
		}
	});
});

// Обробка отриманого контакту
bot.on('message:contact', async (ctx) => {
	const contact = ctx.message.contact;
	console.log(`Номер телефону: ${contact.phone_number}`);

	// Перевірка користувача
	const authUser = await checkUser(contact.phone_number, users);

	if (authUser) {
		// Якщо номер телефону є в базі даних, встановіть стан користувача як "авторизований"
		userStates[ctx.from.id] = {
			isAutorized: true,
			userName: authUser.userName,
			filesToSend: [],
			marker: null,
			userData: {
				fop: '',
				counterparyName: '',
				isNetwork: '',
				networkName: '',
				payment: '',
			}
		};

		await ctx.reply('Ви успішно авторизувалися. Тепер ви маєте доступ до всіх функцій бота.\nОберіть необхідний пункт з меню.', {
			reply_markup: {
				keyboard: [
					[{ text: "Договір" }],
				],
				resize_keyboard: true,
				one_time_keyboard: true,
			}
		});
	} else {
		await ctx.reply(`На жаль, ми не змогли знайти ваш номер телефону ${contact.phone_number} в нашій базі даних.`);
	}

});

// Якщо користувач вибрав пункт меню "Договір". 
// Вивід меню для вибору ФОПів
bot.hears("Договір", async (ctx) => {
	if (userStates[ctx.from.id].isAutorized === true) {
		const rows = fopLabels.map(label => [Keyboard.text(label)]);
		const fopKeyboard = Keyboard.from(rows).resized().oneTime();

		await ctx.reply("Оберіть від кого потрібно укласти договір:", {
			reply_markup: fopKeyboard
		});

	} else {
		ctx.reply('Вибачте, але ви повинні авторизуватися, щоб отримати доступ до цієї команди.');
	}

});

// Після вибору нашого ФОПа. 
// Запит в користувача найменування контрагента
bot.hears(fopLabels, async (ctx) => {
	if (userStates[ctx.from.id].isAutorized === true) {
		userStates[ctx.from.id].userData.fop = ctx.message.text;
		userStates[ctx.from.id].marker = 'counterpartyName';
		await ctx.reply("Напишіть найменування ФОПа чи юридичної особи, з якою потрібно укласти договір");
	} else {
		ctx.reply('Вибачте, але ви повинні авторизуватися, щоб отримати доступ до цієї команди.');
	}
});

// Якщо користувач хоче вказати свій варіант оплати
bot.hears("Вказати свій варіант", async (ctx) => {
	if (userStates[ctx.from.id].isAutorized === true) {
		userStates[ctx.from.id].userData.payment = '';
		userStates[ctx.from.id].marker = 'paymentOther';
		await ctx.reply("Напишіть, який має бути строк оплати");
	} else {
		ctx.reply('Вибачте, але ви повинні авторизуватися, щоб отримати доступ до цієї команди.');
	}
});

// Після введення користувачем строку оплати.
// Запит в користувача про те, чи це торгова мережа.
// Вивід меню для вибору торгової мережі
bot.hears(paymentLabels, async (ctx) => {
	if (userStates[ctx.from.id].isAutorized === true) {
		userStates[ctx.from.id].userData.payment = ctx.message.text;
		const yesNoKeyboard = new Keyboard().text("Так - це мережа").text("Ні - це не мережа").resized().oneTime();

		await ctx.reply("Це торгова мережа (сітка)?", {
			reply_markup: yesNoKeyboard
		});
	} else {
		ctx.reply('Вибачте, але ви повинні авторизуватися, щоб отримати доступ до цієї команди.');
	}
});

// Якщо користувач вказав, що контрагент - це торгова мережа. 
// Запит в користувача назви мережі
bot.hears("Так - це мережа", async (ctx) => {
	if (userStates[ctx.from.id].isAutorized === true) {
		userStates[ctx.from.id].userData.isNetwork = true;
		userStates[ctx.from.id].marker = 'networkName';
		await ctx.reply("Вкажіть назву торгової мережі (сітки):");
	} else {
		ctx.reply('Вибачте, але ви повинні авторизуватися, щоб отримати доступ до цієї команди.');
	}
});

// Якщо користувач вказав, що контрагент - це не торгова мережа.
// Запит в користувача назви мережі
bot.hears("Ні - це не мережа", async (ctx) => {
	if (userStates[ctx.from.id].isAutorized === true) {
		userStates[ctx.from.id].userData.isNetwork = false;

		await ctx.reply("Додайте установчі документи контрагента: \n1.витяг / виписка з ЄДР, \n2.витяг платника єдиного податку (якщо платник),\n3.витяг платника ПДВ (якщо платник),\n4.паспорт + ідентифікаційний код (для ФОП)");
	} else {
		ctx.reply('Вибачте, але ви повинні авторизуватися, щоб отримати доступ до цієї команди.');
	}
});

bot.hears("Відправити заявку на договір", async (ctx) => {
	if (userStates[ctx.from.id].isAutorized === true) {

		// Відправити заявку на електронну пошту
		sendEmailWithAttachment(userStates[ctx.from.id]);

		await ctx.reply(`Заявку на укладення договору з ${userStates[ctx.from.id].userData.counterparyName} надіслано.`, {
			reply_markup: {
				keyboard: [
					[{ text: "Договір" }],
				],
				resize_keyboard: true,
				one_time_keyboard: true,
			}
		});
	} else {
		ctx.reply('Вибачте, але ви повинні авторизуватися, щоб отримати доступ до цієї команди.');
	}
});

bot.hears("Не відправляти заявку на договір", async (ctx) => {
	if (userStates[ctx.from.id].isAutorized === true) {
		userStates[ctx.from.id].userData = {
			fop: '',
			counterparyName: '',
			isNetwork: '',
			networkName: '',
			payment: '',
		}
		userStates[ctx.from.id].marker = null;

		userStates[ctx.from.id].filesToSend = []

		const rows = menuLabels.map(label => [Keyboard.text(label)]);
		const menuKeyboard = Keyboard.from(rows).resized().oneTime();

		await ctx.reply("Бот запущений! \nОберіть необхідний пункт з меню.", {
			reply_markup: menuKeyboard
		});
	} else {
		ctx.reply('Вибачте, але ви повинні авторизуватися, щоб отримати доступ до цієї команди.');
	}
});

bot.on("message:text", async (ctx) => {
	if (userStates[ctx.from.id].isAutorized === true) {

		switch (userStates[ctx.from.id].marker) {
			case "paymentOther":
				userStates[ctx.from.id].userData.payment = ctx.message.text;
				const yesNoKeyboard = new Keyboard().text("Так - це мережа").text("Ні - це не мережа").resized().oneTime();

				await ctx.reply("Це торгова мережа (сітка)?", {
					reply_markup: yesNoKeyboard
				});
				break;
			case "counterpartyName":
				userStates[ctx.from.id].userData.counterparyName = ctx.message.text;

				const rows = paymentLabels.map(label => [Keyboard.text(label)]);
				const paymentsKeyboard = Keyboard.from(rows).resized().oneTime();

				await ctx.reply("Який строк оплати? (оберіть один з варіантів)", {
					reply_markup: paymentsKeyboard
				});
				break;
			case "networkName":
				userStates[ctx.from.id].userData.networkName = ctx.message.text;
				await ctx.reply("Додайте установчі документи контрагента: \n1.витяг / виписка з ЄДР, \n 2.витяг платника єдиного податку,\n3.витяг платника ПДВ,\n4.паспорт + ідентифікаційний код");
				break;
		}
	} else {
		ctx.reply('Вибачте, але ви повинні авторизуватися, щоб отримати доступ до цієї команди.');
	}
});

// Обробник події для отримання зображень від користувачів
bot.on(":photo", async (ctx) => {
	if (userStates[ctx.from.id].isAutorized === true) {
		const photoArr = ctx.message.photo;

		// Отримуємо найбільший розмір фотографії
		const originalSizePhoto = photoArr.reduce((prev, current) => {
			return (prev.width > current.width) ? prev : current;
		});

		// Отримуємо URL оригінального розміру фотографії
		const file = await ctx.api.getFile(originalSizePhoto.file_id);
		const originalPhotoUrl = `https://api.telegram.org/file/bot${bot.token}/${file.file_path}`;

		userStates[ctx.from.id].filesToSend.push({ path: originalPhotoUrl });

		const sendKeyboard = new Keyboard().text("Відправити заявку на договір").text("Не відправляти заявку на договір").resized().oneTime();

		await ctx.reply("Фото/файл завантажено", {
			reply_markup: sendKeyboard
		});
	} else {
		ctx.reply('Вибачте, але ви повинні авторизуватися, щоб отримати доступ до цієї команди.');
	}


});

// Обробник події для отримання документів від користувачів
bot.on(":document", async (ctx) => {
	if (userStates[ctx.from.id].isAutorized === true) {
		const file = await bot.api.getFile(ctx.message.document.file_id);
		const fileUrl = `https://api.telegram.org/file/bot${bot.token}/${file.file_path}`;

		userStates[ctx.from.id].filesToSend.push({ path: fileUrl });

		const sendKeyboard = new Keyboard().text("Так. Відправити заявку на договір.").text("Не відправляти заявку на договір").resized().oneTime();

		await ctx.reply("Відправити заявку на укладення договору?", {
			reply_markup: sendKeyboard
		});
	} else {
		ctx.reply('Вибачте, але ви повинні авторизуватися, щоб отримати доступ до цієї команди.');
	}
});

/* =========== Налаштування поштового сервера + функція для відправки листа =========== */

// Конфігурація Nodemailer
const transporter = nodemailer.createTransport({
	host: "smtp.gmail.com",
	port: 587,
	secure: false, // Use `true` for port 465, `false` for all other ports
	auth: {
		user: "verest.juryst@gmail.com",
		pass: "kmcj yaps ydbm pspd",
	},
});

// Функція для надсилання електронного листа з вкладенням
function sendEmailWithAttachment(userState) {
	console.log(userState);
	const mailOptions = {
		from: "verest.juryst@gmail.com",
		to: "verest.juryst@gmail.com",
		subject: `Заявка на договір: ${userState.userName}`,
		text: `Контрагент: ${userState.userData.counterparyName} ;\nФОП: ${userState.userData.fop};\nТоргова мережа?: ${userState.userData.isNetwork};\nНазва торгової мережі: ${userState.userData.networkName};\nСтрок оплати: ${userState.userData.payment};\n`,
		attachments: userState.filesToSend,
	};

	transporter.sendMail(mailOptions, (error, info) => {
		if (error) {
			console.error(error);
		} else {
		}
	});
}

// Функція перевірки чи є користувач в базі даних
function checkUser(number, users) {
	let phoneNumber = null;

	// Видаляємо "+" з початку номера, якщо він є
	if (number.startsWith('+')) {
		phoneNumber = number.slice(1);
	} else {
		phoneNumber = number;
	}

	const user = users.find(user => user.userPhone === phoneNumber);

	return user ? user : false;
}

/* =========== Відлов помилок =========== */

bot.catch((err) => {
	const ctx = err.ctx;
	console.error(`Виникла помилка ${ctx.update.update_id}:`);
	const e = err.error;

	if (e instanceof GrammyError) {
		console.error("Error in request: ", e.description);
	} else if (e instanceof HttpError) {
		console.error("Cloud not contact Telegram: ", e);
	} else {
		console.error("Unknown error: ", e);
	}
});


bot.start();